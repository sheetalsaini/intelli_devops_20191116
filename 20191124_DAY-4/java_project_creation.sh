
# 2.3 Create a Java Project :
    
STEP-1 : Go to start.spring.io 

Project Name : aws-reinvent

STEP-2 : Use IDE Tool Eclipse/STS/VSTS/Pycharm etc....

IDE Tool : STS 

Import a Project i.e. aws-reinvent 

STEP-3 : Code :

        1. Server Side Coding : Java 
        2. Client Side Coding : HTML, CSS, JS etc.. 
        3. DB Connections     : RDBMS(Oracle, MySQL) / NOSQL(DynamoDB) 

STEP-4 : Maven Goals 

    # mvn clean
    # mvn clean install 
    # mvn clean install package 
    # mvn clean install package deploy
    # mvn clean release:clear release:plan release:deploy 

    Code  : aws-reinvent.war 
    Guide : Step by Step Deployment Document  


Copy the Code From Github To GitLab :

  534  git clone https://gitlab.com/cloudbinary/aws-reinvent.git
  535  pwd
  536  cd aws-reinvent/
  537  ls -lrta
  538  cp -pvr /Users/keshavkummari/Downloads/java_code_0001-master.zip .
  539  ls -lrta
  540  unzip java_code_0001-master.zip 
  541  pwd
  542  ls -lrta
  543  rm -f java_code_0001-master.zip 
  544  pwd
  545  ls -lrta
  546  cd java_code_0001-master/
  547  ls -lrt
  548  cp -pvr * ../
  549  pwd
  550  ls -lrt
  551  cd ../
  552  pwd
  553  ls -lrta
  554  rm -rf java_code_0001-master/
  555  pwd
  556  ls -lrta
  557  git status
  558  git add .
  559  git status
  560  git commit -m "JavaCode"
  561  git push -u origin master
  562  pwd
  