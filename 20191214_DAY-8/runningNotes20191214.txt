Agenda :

1. Kubernetes 

    1. Introduction to Kubernetes :
        - It's Open Source 
        - Container Orchestration Software 
        - Originally it was developed by Google 
        - K8s was denoted to Cloud Native Computing Founcation(CNCF)
        - First Version was released on 21st July 2015
    1.1 Features of Kubernetes :
        1.1.1 PODS
        1.1.2 Replication Controller
        1.1.3 Service Discovery
        1.1.4 Networking
        1.1.5 Storage Management 
        1.1.6 Secret Management
        1.1.7 Resource Monitoring
        1.1.8 Rolling Updates
        1.1.9 Health Checks 


    2. Docker Swarm VS Kubernetes :
        - Docker Swarm is Open Source Platform
        - Manager Nodes
        - Worker Nodes 
        - Services 
        - Tasks

    GUI
    Load Balancing    

    Nodes : 

    Docker Swarm : 2000 nodes 
    Kubernetes   : 5000 

    Rollbacks : 

 

    3. Kubernetes Architecture

    Master Node :
        1. etcd 
        2. API Server 
        3. Scheduler 
        4. Controller Manager 

    Slave Node :
        1. Kubelet 
        2. Kube-Proxy 

    4. Kubernetes Installation

    1. Bare Metal Installation : kubeadm 

    2. Virtualization Environment : minikube 

    3. Kubernetes on AWS : kops 

    4. Kubernetes on Google Cloud Platform  : Kubernetes
    


    5. Working of Kubernetes
    6. Deployments in Kubernetes
    7. Services in Kubernetes
    8. Ingress in Kubernetes 
    9. Kubernetes Dashboard 

2. Docker Networking
