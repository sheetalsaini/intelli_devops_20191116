# Steps for Master 

Step 1:  We will create a nodeport service for our nginx deployment, run the following command:

# kubectl create service nodeport nginx –tcp=80:80

Step 2: Note down the Nodeport for this service by running the command, 

# kubectl get svc nginx

Step 3: Verify the working of the service by browsing to the IP address of the master or slave with this port

Go to the Browser and Verify 

http://ipaddress:32396

