Agenda : 

#------------------------------------------------------------------------#

#------------------------------------------------------------------------#

1. Kuberentes Installation

2. Creating a Deployment

3. Creating Service 

4. Creating an Ingress

5. Kuberentes Dashboard 


kubectl scale --current-replicas=3 --replicas=4 deployment/nginx-deployment

http://ec2-34-235-159-250.compute-1.amazonaws.com:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/.

https://github.com/kubernetes/dashboard

ubuntu@k8s-master:~$ history
    1  sudo su
    2  sudo kubeadm init --pod-network-cidr=172.16.0.0/16
    3  pwd
    4  ls -lrta
    5  mkdir -p $HOME/.kube
    6  ls -lrta
    7  ls -lrt /etc/kubernetes/admin.conf 
    8  wc /etc/kubernetes/admin.conf
    9  sudo wc /etc/kubernetes/admin.conf
   10  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
   11  ls -lrta
   12  tree .kube/
   13  cat .kube/config 
   14  sudo cat .kube/config
   15  kubectl get-pods
   16  kubectl get nodes
   17  kubectl get-pods
   18  kubectl get nodes
   19  kubectl get pods --all-namespaces
   20  sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
   21  kubectl get nodes
   22  kubectl get pods --all-namespaces
   23  kubectl get nodes
   24  kubectl get pods --all-namespaces
   25  vi nginx
   26  mv nginx nginx.yml
   27  ls -lrt 
   28  cat nginx.yml 
   29  kubectl get nodes
   30  kubectl get pods --all-namespaces
   31  docker --version
   32  sudo systemctl status docker.service
   33  kubectl create -f nginx.yml 
   34  kubectl get pods
   35  kubectl get pods --all-namespaces
   36  docker ps
   37  sudo docker ps
   38  dockerimages
   39  docker images
   40  sudo docker images
   41  hostname
   42  docker images
   43  sudo docker images
   44  sudo docker ps
   45  kubectl get pods
   46  kubectl get pods -o wides
   47  kubectl get nodes
   48  kubectl get po nginx-deployment-54f57cf6bf-8d2ct
   49  hostname
   50  kubectl get pods -o wide
   51  curl 172.16.2.7
   52  kubectl describe pods
   53  kubectl get pods -o wide
   54  kubectl describe pods/nginx-deployment-54f57cf6bf-8d2ct
   55  curl 172.31.39.192
   56  curl 172.31.39.192:80
   57  kubectl describe nodes
   58  # kubectl describe nodes
   59  kubectl get nodes
   60  kubectl describe nodes k8s-node1.devops.com
   61  kubectl get ds
   62  kubectl get pods -o wide
   63  kubectl exec nginx-deployment-54f57cf6bf-8d2ct date
   64  kubectl exec nginx-deployment-54f57cf6bf-8d2ct df -TH
   65  kubectl exec nginx-deployment-54f57cf6bf-8d2ct who
   66  kubectl exec -it nginx-deployment-54f57cf6bf-8d2ct /bin/bash
   67  kubectl logs nginx-deployment-54f57cf6bf-8d2ct
   68  kubectl logs -f nginx-deployment-54f57cf6bf-8d2ct
   69  kubectl plugin list
   70  kubectl get services
   71  kubectl get nodes
   72  kubectl get pods --all-namespaces
   73  kubectl get pods -o wide
   74  kubectl get deployment nginx-deployment
   75  kubectl get pod nginx-deployment -o yaml
   76  kubectl get pod nginx-deployment -o nginx.yml 
   77  kubectl get pod nginx-deployment-54f57cf6bf-8d2ct -o yaml
   78  pwd
   79  tree
   80  cat nginx.yml 
   81  kubectl get pod nginx-deployment-54f57cf6bf-8d2ct -o yaml
   82  kubectl get deployment nginx-deployment
   83  kubectl get pods -o wide
   84  kubectl get pods --show-labels
   85  kubectl get pods -o json | jq '.items[].spec.containers[].env[]?.valueFrom.secretKeyRef.name' | grep -v null | sort | uniq
   86  kubectl get nodes
   87  kubectl get pods
   88  kubectl scale --replicas=4 nginx-deployment
   89  history | grep deploy
   90  kubectl get deployment nginx-deployment
   91  kubectl scale --current-replicas=3 --replicas=4 nginx-deployment
   92  kubectl scale --current-replicas=3 --replicas=4 nginx-deployment/nginx
   93  kubectl scale --current-replicas=3 --replicas=4 deployment/nginx
   94  kubectl scale --current-replicas=3 --replicas=4 deployment/nginx-deployment
   95  kubectl get pods
   96  kubectl get pods -o wide
   97  kubectl scale --current-replicas=4 --replicas=2 deployment/nginx-deployment
   98  kubectl get pods -o wide
   99  kubectl cluster-info
  100  curl https://172.31.46.123:6443
  101  curl http://172.31.46.123:6443
  102  kubectl get pods -o wide
  103  kubectl api-resources
  104  kubectl api-resources -o wide
  105  kubectl help
  106  kubectl get pods
  107  kubectl get nodes
  108  kubectl get pods
  109  kubectl get service


#------------------------------------------------------------------------#

#------------------------------------------------------------------------#