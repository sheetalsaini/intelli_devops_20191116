Software Development Life Cycle:

1. Lean Model (7 Lean Principles: 1. Eliminate Waste, Amplify Learning, Decide As late Possible, Deliver as fast as possible, empower the team, & build integrity)
2. WaterFall 
3. Agile etc....

Customer :  Idea ---- Production (Banking Domain: OnlineBankingService )

IT Service :
    1. Project Manager:
        1. Development  : Developers (CC, CD, CI )
        2. QA           : Manual Or Automation (CT)
        3. OPS          : SysAdmins, DB Admin, Ops etc... (CD,CM,CF,CTroubleshootin)
Customer: PO ---> Scrum Master ---> Backlogs(Jira) ---> Scrum Rituals:
                    1. Sprint Planning : Min 2 Weeks and Max 4 Weeks 
                    2. Sprint Daily Standup - EveryDay 5 Days Week @ Start of the Day : 15 Minutes
                    3. Refinement : Current Sprint Refined or Backlogs Refinement
                    4. Review & Restrospective : 
                    5. End the Sprint : Burn Down/Up Charts 
    1. Home Page 
    2. Registration
    3. Login page 
    4. User Dashboard : Account Balance 
    5. Single Sign On 
    6. MFA 
    7. etc....

Stages:
    1. Developers   : dev.ckk.com 
    2. Testers      : tst.ckk.com 
    3. Business/Ops : acc.ckk.com 
    4. Production   : prd.ckk.com   https://ckk.com  
